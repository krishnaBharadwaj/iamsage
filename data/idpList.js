let idplist = [
	{
        name: "UIC",
        value: "https://shibboleth.uic.edu/shibboleth"
    }, {
        name: "Google",
        value: "https://accounts.google.com/o/oauth2/auth"
    }, {
        name: "GitHub",
        value: "https://github.com/login/oauth/authorize"
    }
];

module.exports = idplist;