const express = require("express");
const path = require("path");
const json5 = require("json5");
const appRoot = require("app-root-path");
const fs = require("fs");
const ensureLogin = require("../src/ensureLogin");
const router = express.Router();
const dataFolder = path.join(appRoot.toString(), "data");
const crypto = require("crypto");



let config = global.config;
let eventDB, roleDB, userDB = null;
router.use((req, res, next) => {
	if (userDB === null) {
		userDB = req.app.get("userdb");
		eventDB = req.app.get("eventdb");
		roleDB = req.app.get("roledb");
	}
	return next();
});
router.use(ensureLogin);


router.get('/event/:eventId', (req, res, next) => {
	console.log("Params:", req.params);
	eventDB.get(encodeURIComponent(req.params.eventId), (ecount, event) => {
		console.log(ecount);
		if (ecount > 0) {
			//Check if user satisfies event membership criteria

			var uid = createHash(req.user.userinfo.sub);
			userDB.get(uid, (ucount, user) => {
				if (ucount > 0) {
					if (user.hasOwnProperty("events") === false) {
						user.events = {};
					}
					if (user.events.hasOwnProperty(event.id) === false) {
						user.events[event.id] = event.defaultRoleId || "System_Viewer";
						userDB.set(uid, user, err => {
							if (err) {
								console.log(err);
								res.status(404);
							}
						});
						event.users[uid] = event.defaultRoleId || "System_Viewer";
						eventDB.set(event.id, event,  err => {
							if (err) {
								console.log('Error in saving event membership details:', err);
								res.status(404);
							} else {
								console.log(event);
								// Add a notification message indicating the user has been added to the Event.
								res.redirect("/home?id=" + encodeURIComponent(uid));
							}
						});
					} else {
						//User already is a member of the event
						//So redirect to home without doing anything.
						res.redirect("/home?id=" + encodeURIComponent(uid));
					}
				} else {
					//Handle error
				}
			});
		}
	});
});

module.exports = router;
