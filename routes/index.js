const express = require("express");
const path = require("path");
const appRoot = require("app-root-path");
const router = express.Router();


let idplist = require("../Data/idpList");
let config = global.config;


let idpUrls = idplist.map(idp => {
	return {
		name: idp.name,
		value: "https://" + config.host + ':' + config.port + "/login" + idp.name
	};
});

idpUrls.push({
	name: "CILogon",
	value: "https://" + config.host + ':' + config.port + "/login"
});

idpUrls.push({
	name: "Test User 1",
	value: "https://" + config.host + ':' + config.port + "/loginDummy1"
});

idpUrls.push({
	name: "Test User 2",
	value: "https://" + config.host + ':' + config.port + "/loginDummy2"
});

router.get("/options",  async (req, res) => {
	try {
		res.status(200).json({
			data: idpUrls
		});
	} catch(error) {
		res.status(400).json({
			message: "Some error occured",
			error
	    });
	}
});

router.get("/", async (req, res) => {

	try {
		
		var entryFilePath = path.join(appRoot.toString(), "iamsageview", "dist", "index.html");
		console.log(entryFilePath);
		res.sendFile(entryFilePath);
	} catch(error) {
		res.status(400).json({
			message: "Some error occured",
			error
	    });
	}
});


module.exports = router;