const express = require("express");
const router = express.Router();

let users = require("../Data/database");

router.get("/list", async (req, res) => {
  try {
    res.status(200).json({
      data: users
    });
  } catch (err) {
    res.status(400).json({
      message: "Some error occured",
      err
    });
  }
});

router.get("/:id", async (req, res) => {
  let { id } = req.params;
  id = Number(id);
  try {
    let user = users.find(user => user.id === id);
    res.status(200).json({
      data: user
    });
  } catch (err) {
    res.status(400).json({
      message: "Some error occured",
      err
    });
  }
});

module.exports = router;