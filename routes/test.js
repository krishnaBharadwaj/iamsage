const express = require("express");
const path = require("path");
const json5 = require("json5");
const appRoot = require("app-root-path");
const fs = require("fs");
const ensureLogin = require("../src/ensureLogin");
const router = express.Router();
const dataFolder = path.join(appRoot.toString(), "data");
const crypto = require("crypto");
const {DataJSONStore} = require("../src/store.js");

let sageactions = require("../Data/sageActions");
let useractions = require("../Data/userActionTable");
let config = global.config;

let sageEvents = {};
let eventDB, roleDB, userDB = null;
router.use((req, res, next) => {
	if (userDB === null) {
		userDB = req.app.get("userdb");
		eventDB = req.app.get("eventdb");
		roleDB = req.app.get("roledb");
	}
	return next();
});
router.use(ensureLogin);

router.get("/", (req, res) => {
	
	return res.sendFile(path.join(appRoot.toString(), "iamsageview", "dist", "test.html"))
});

router.get("/uname", (req, res) => {
	console.log(req.query);
	userDB.get(req.user.id, function (err, user) {
		if (err) {
			res.status(404);
		} else {
			res.status(200).json({
				data: user.userinfo.name
			});
		}
		
	});	
});

router.get("/allusers", (req, res) => {
	userDB.all((err, users) => {
		if (err) {
			res.status(404);
		} else {
			if (req.query.id !== null && req.query.id !== undefined) {
				/*var result = events.filter(e => {
					return e.
				});*/
			}
			var userInfoList = users.map(u => {return u.userinfo});
			console.log("users:", userInfoList);
			res.status(200).json({
				data: userInfoList
			});
		} 
	});	
});

router.get("/sageactions", (req, res) => {
	res.status(200).json({
		data: sageactions
	});
});


router.get("/useractiontable", (req, res) => {
	res.status(200).json({
		data: useractions
	});
});


router.post("/setuserrole", (req, res) => {
});

router.post("/userroledef", (req, res) => {
	//console.log(req.body);
	var newRole = {
		name: req.body.name,
        actionList: [...req.body.actionList],
        createdBy: req.user.userinfo.name
    };
	var id = createHash(req.user.userinfo.sub);
	id = encodeURIComponent(id + "_" + newRole.name);
	roleDB.get(id, (count, role) => {
		if (count === 0) {
			roleDB.set(id, newRole,  err => {
				if (err) {
					console.log('Error in creating new user role:', err);
					res.status(404);
				} else {
					console.log(newRole);
					res.status(200).json({
						data: newRole
					});
				}
			});
		} else {
			console.log(role);
			console.log("Duplicate");
			res.status(200).json({
				message: "duplicate entry"
			});
		}
	})
	
});

module.exports = router;