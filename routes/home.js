const express = require("express");
const path = require("path");
const json5 = require("json5");
const appRoot = require("app-root-path");
const fs = require("fs");
const ensureLogin = require("../src/ensureLogin");
const router = express.Router();
const dataFolder = path.join(appRoot.toString(), "data");
const crypto = require("crypto");



let config = global.config;


let eventDB, roleDB, userDB = null;
router.use((req, res, next) => {
	if (userDB === null) {
		userDB = req.app.get("userdb");
		eventDB = req.app.get("eventdb");
		roleDB = req.app.get("roledb");
	}
	return next();
});
router.use(ensureLogin);

router.get("/", (req, res) => {
	return res.sendFile(path.join(appRoot.toString(), "iamsageview", "dist", "home.html"))
});

router.get("/uname", (req, res) => {
	console.log(req.query);
	userDB.get(req.query.id, function (count, user) {
		if (count === 0) {
			res.status(404);
		} else {
			res.status(200).json({
				data: user.userinfo.name
			});
		}
		
	});	
});

router.get("/sageevents", (req, res) => {
	console.log(req.query.id);
	userDB.get(req.query.id, (ucount, user) => {
		console.log("Eids:");
		if (ucount > 0) {
			var eids = Object.keys(user.events);
			console.log(eids);
			eventDB.get(...eids, (ecount, events) =>{
				if (ecount > 0) {
					res.status(200).json({
						data: events
					});
				} else {
					res.status(404);
				}
			});
		}
	});
});

router.post("/newsageevent", (req, res) => {
	//console.log(req.body);
	var newEvent = {
		name: req.body.name,
		description: req.body.description,
        scheduleType: req.body.scheduleType,
        schedule: req.body.schedule,
        accessType: req.body.accessType,
        display: req.body.display,
        createdBy: req.user.userinfo.name,
        defaultRoleId: req.body.defaultRoleId,
        shareUrl: "",
        users: {

        }
    };
	var uid = createHash(req.user.userinfo.sub);
	var eid = encodeURIComponent(uid + "_" + newEvent.name);

	newEvent.shareUrl = "https://" + config.host + ":" + config.port + "/share/event/" + eid;
	// Name should be replaced by schedule for uniqueness
	eventDB.get(eid, (count, event) => {
		if (count === 0) {
			newEvent.users[uid] = "System_Owner";
			userDB.get(uid, (count, user) =>{
				if (user.hasOwnProperty("events") === false) {
					user.events = {};
				}
				user.events[eid] = "System_Owner";
				userDB.set(uid, user, err => {
					if (err) {
						console.log(err);
					}
				})
			});
			eventDB.set(eid, newEvent,  err => {
				if (err) {
					console.log('Error in creating new sage event:', err);
					res.status(404);
				} else {
					console.log(newEvent);
					res.status(200).json({
						data: newEvent
					});
				}
			});
		} else {
			console.log(event);
			console.log("Duplicate");
			res.status(200).json({
				message: "duplicate entry"
			});
		}
	});
	
});



router.get("/removeEvent", (req, res) => {

});
module.exports = router;