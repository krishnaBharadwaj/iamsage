/**
 * Express middleware for making sure that the user is logged in
 *
 * @method ensureAuthenticated
 * @param req {Object} the request object
 * @param res {Object} the response object
 * @param next {Object} the middleware chaining object
 */
function ensureAuthenticated(req, res, next) {
	console.log("Auth check:" + req.isAuthenticated());
	// Pass through is the request has been authenticated
	if (req.isAuthenticated()) {
		return next();
	}

	// Otherwise save the url for redirection after authenticaion and redirect to login page
	req.session.returnTo = req.originalUrl;
	return res.redirect('/index');
}

module.exports = ensureAuthenticated;