// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014

/**
 * SAGE2 Session and User Database
 *
 * @module server
 * @submodule store
 * @requires express-session, json5, fs
 */

// require variables to be declared
"use strict";

const {Store} = require('express-session');
var json5 = require('json5');
var fs = require('fs');


class SessionJSONStore extends Store {
	constructor() {
		super();
	}
	initialize(path) {
		this.filePath = path;
		if (fs.existsSync(path)) {
			var jsonString = fs.readFileSync(this.filePath, 'utf8');
			this.db = json5.parse(jsonString);
		} else {
			this.db = {};
			fs.writeFileSync(this.filePath, json5.stringify(this.db));
		}
	}
	save(cb) {
		try {
			fs.writeFile(this.filePath, json5.stringify(this.db), function(error) {
				if (error) {
					cb("Could not save database " + error);
				} else {
					cb(null);
				}
			});
			
		} catch (error) {
			cb("Could not save database " + error);
		}
	}
	all(cb) {
		cb("", Object.values(this.db));
	}
	destroy(sid, cb) {
		if (this.db.hasOwnProperty(sid) === true) {
			delete this.db[sid];
			this.save(cb);
		} else {
			cb(null);
		}
	}
	clear(cb) {
		this.db = {};
		this.save(cb);
	}
	length(cb) {
		try {
			var count = Object.keys(this.db).length;
			cb("", count);
		} catch (error) {
			cb(error);
		}
	}
	get(sid, cb) {
		try {
			if (this.db.hasOwnProperty(sid) === true) {
				cb("", this.db[sid]);
			} else {
				cb(null, null);
			}
		} catch (error) {
			cb(error, undefined);
		}
	}
	set(sid, session, cb) {
		this.db[sid] = cloneObject(session);
		this.db[sid].sid = sid;
		this.save(cb);
	}
	touch(sid, session, cb) {
		try {
			session.touch();
			this.db[sid] = cloneObject(session);
			this.db[sid].sid = sid;
			this.save(cb);
		} catch (error) {
			cb(error);
		}
	}
}

class DataJSONStore {
	initialize(path) {
		this.filePath = path;
		if (fs.existsSync(path)) {
			var jsonString = fs.readFileSync(this.filePath, 'utf8');
			this.db = json5.parse(jsonString);
		} else {
			this.db = {};
			fs.writeFileSync(this.filePath, json5.stringify(this.db));
		}
	}
	save(cb) {
		try {
			fs.writeFile(this.filePath, json5.stringify(this.db), function(error) {
				if (error) {
					console.log(error);
				} else {
					cb(null);
				}
			});
			
		} catch (error) {
			cb("Could not save database " + error);
		}
	}
	all(cb) {
		cb("", Object.values(this.db));
	}
	destroy(id, cb) {
		if (this.db.hasOwnProperty(id) === true) {
			delete this.db[id];
			this.save(cb);
		} else {
			cb("Data entry does not exist:" + id);
		}
	}
	clear(cb) {
		this.db = {};
		this.save(cb);
	}
	length(cb) {
		try {
			var count = Object.keys(this.db).length;
			cb("", count);
		} catch (error) {
			cb(error);
		}
	}
	get(...idListAndCb) {
		let cb = idListAndCb.splice(idListAndCb.length - 1, 1);
		cb = cb[0];
		var objList = idListAndCb.map(x => {
			return (this.db.hasOwnProperty(x) === true)? this.db[x] : null;
		});
		cb(objList.filter(x => { return x !== null;}).length, (objList.length === 1) ? objList[0] : objList);
	}
	set(id, data, cb) {
		console.log(data);
		this.db[id] = cloneObject(data);
		this.db[id].id = id;
		this.save(cb);
	}
	reload() {
		var jsonString = fs.readFileSync(this.filePath, 'utf8');
		this.db = json5.parse(jsonString);
	}
}

function cloneObject(obj) {
	if (!obj) {
		return obj;
	}
	let v;
	let clone = Array.isArray(obj) ? [] : {};
	for (const i in obj) {
		v = obj[i];
		clone[i] = (typeof v === "object") ? cloneObject(v) : v;
	}
	return clone;
}

module.exports.SessionJSONStore = SessionJSONStore;
module.exports.DataJSONStore = DataJSONStore;
