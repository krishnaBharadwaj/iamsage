// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014

/**
 * Database Manager
 *
 * @module server
 * @submodule DatabaseManager
 * @requires json5, fs
 */

// require variables to be declared
"use strict";


var json5 = require('json5');
var fs = require('fs');

function DatabaseManager() {
	
};

DatabaseManager.prototype.initialize = function(path) {
	this.filePath = path;
	var schema, database;
	if (fs.existsSync(path)) {
		var jsonString = fs.readFileSync(this.filePath, 'utf8');
		({schema: this.schema, database: this.database} = json5.parse(jsonString));
	} else {
		this.database = {};
		this.schema = {};
		fs.writeFileSync(this.filePath, json5.stringify({schema: this.schema, database: this.database}));
	}
};

DatabaseManager.prototype.createTable = function(tableName, attributeObject) {
	try {
		if(attributeObject.hasOwnProperty("id") === false) {
			throw "Cannot create table without unique id attribute";
		} else if (this.hasTable(tableName)) {
			throw "Duplicate entry! Table with name: " + tableName + ", already exists";
		}
		this.database[tableName] = {};
		this.schema[tableName] = attributeObject;
		this.save();
	} catch(err) {
		console.log(err);
	}
	return this;
};





DatabaseManager.prototype.insertRow = function(tableName, row) {
	try {
		var keys = Object.keys(this.schema[tableName]);
		var insertRow = {};
		if (row.hasOwnProperty("id") === false) {
			throw "Cannot insert into " + tableName + ", row id missing!";
		} else if (this.database[tableName].hasOwnProperty(row.id) === true) {
			throw "Duplicate entry! Row with id: " + row.id + ", already exists";
		}
		keys.forEach(k => {
			if (row.hasOwnProperty(k)) {
				insertRow[k] = row[k];
			} else {
				throw "Missing value for attribute: " + k;
			}
		});
		this.database[tableName][row[id]] = insertRow;
		this.save();
	} catch(err) {
		console.log(err);
	}
	return this;
};

DatabaseManager.prototype.select = function(attributeObject, tableName, filterFunc) {
	try {
		var keys = Object.keys(attributeObject);
		var rows = Object.values(this.database[tableName]);
		
		var attributeCount = 0;
		keys.forEach(k => {
			if (this.schema[tableName].hasOwnProperty(k) === false) {
				throw "Table " + tableName + " does not have a field labelled " + k;
			}
			attributeCount++;
		});
		if (attributeCount > 0 && attributeCount !== Object.keys(this.schema[tableName]).length ) {
			rows = rows.map(r => {
				return  (({ ...keys }) => ({ ...keys }))(r);
			});
		}
		if (filterFunc) {
			rows = rows.filter(filterFunc);	
		}
		
		return rows;
	} catch(err) {
		console.log(err);
	}
	return [];
};

DatabaseManager.prototype.deleteRow = function(tableName, id) {
	try {
		var table = this.database[tableName];
		delete table[id];
		this.save();
	} catch(err) {
		console.log(err);
	}
	return this;

};

DatabaseManager.prototype.deleteTable = function(tableName) {
	try {
		delete this.schema[tableName];
		delete this.database[tableName];
		this.save();
	} catch(err) {
		console.log(err);
	}
	return this;
};

DatabaseManager.prototype.save = function() {
	try {
		fs.writeFile(this.filePath, json5.stringify({schema: this.schema, database: this.database}), function(error) {
			if (error) {
				cb("Could not save database " + error);
			}
		});
	} catch (error) {
		cb("Could not save database " + error);
	}
	return this;
};

DatabaseManager.prototype.hasTable = function(tableName) {
	return this.schema.hasOwnProperty(tableName);
};

module.exports = DatabaseManager;