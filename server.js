const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const logger = require("morgan");
const uuid = require("uuid/v4");
const session = require("express-session");
const path = require("path");
const fs = require("fs");
const crypto = require("crypto");
const passport = require("passport");
const appRoot = require("app-root-path");
const { Strategy, Issuer, generators } = require("openid-client");
const { SessionJSONStore, DataJSONStore} = require("./src/store.js");
const DBM = require("./src/DatabaseManager.js");
var http = require('http');
var https = require('https');
var privateKey  = fs.readFileSync('keys/_.evl.uic.edu.key', 'utf8');
var certificate = fs.readFileSync('keys/_.evl.uic.edu.crt', 'utf8');
const MockStrategy = require('passport-mock-strategy');
var credentials = {key: privateKey, cert: certificate};


// your express configuration here







const app = express();


app.use(logger("dev"));
app.use(cors());
app.use('/assets/open', express.static(path.join(appRoot.toString(), "iamsageview", "dist", "assets", "open")));
app.use('/assets/secure', express.static(path.join(appRoot.toString(), "iamsageview", "dist", "assets", "secure")));
app.use('/js', express.static(path.join(appRoot.toString(), "iamsageview", "dist", "js")));


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());



const dataFolder = "data";
const sessionsFolder = path.join(dataFolder, "sessions");


var sessionDB = new SessionJSONStore();
sessionDB.initialize(path.join(sessionsFolder, "sessionDB.json"));

var userDB = new DataJSONStore();
userDB.initialize(path.join(dataFolder, "userDB.json"));


var eventDB = new DataJSONStore();
eventDB.initialize(path.join(dataFolder, "eventDB.json"));


var roleDB = new DataJSONStore();
roleDB.initialize(path.join(dataFolder, "roleDB.json"));

app.set('userdb', userDB);
app.set('eventdb', eventDB);
app.set('roledb', roleDB);



const configFile = fs.readFileSync(path.join("config", "surface.json"));
const config = JSON.parse(configFile);
global.config = config;
// Generate the service worker for caching
//generateSW();

		
var securityFileData = fs.readFileSync(config.security, "ascii");
securityConfig = JSON.parse(securityFileData);
console.log(securityConfig);


const code_verifier = generators.codeVerifier();
// add & configure middleware
app.use(session({
	genid: (req) => {
		return uuid(); // use UUIDs for session IDs
	},
	cookie: {
		secure: true,
		httpOnly: true,
		maxAge: 2 * 24 * 3600 * 1000 // 2 days
	},
	store: sessionDB,
	name: 'SAGE2.sid',
	secret: securityConfig.session_secret,
	resave: false,
	saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
passport.serializeUser(function(user, done) {
	try {
		done(null, user.id);	
	} catch(err) {
		console.log(err, user);
	}
	
});
passport.deserializeUser(function(id, done) {
	userDB.get(id, function (count, user) {
		done(count > 0 ? null : "Not found", user);
	});
});



function verify (tokenset, userinfo, done) {
	var id = createHash(userinfo.sub);
	userDB.get(id, function (count, user) {
		if (count === 0) {
			user = {
				id: id,
				tokenset: tokenset,
				userinfo: userinfo,
				events: {}
			};
		}
		userDB.set(id, user, function(err) {
			if (err) {
				sageutils.log('OIDC', err);
				return done(err, false);
			}
		});
		return done(null, user);
	});
}


function mockVerify (userinfo, done) {
	var id = createHash(userinfo.sub);
	userDB.get(id, function (count, user) {
		if (count === 0) {
			user = {
				id: id,
				tokenset: {},
				userinfo: userinfo,
				events: {}
			};
		}
		userDB.set(id, user, function(err) {
			if (err) {
				sageutils.log('OIDC', err);
				return done(err, false);
			}
		});
		return done(null, user);
	});
}

const redirectToHome = (req, res) => {
	var redirectTo = req.session.returnTo || "/home";
	redirectTo = redirectTo + "?id=" + encodeURIComponent(req.user.id);
	delete req.session.returnTo;
	return res.redirect(redirectTo);
}

//var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

//httpServer.listen(9292);
httpsServer.listen(9090);


let mockUserSuffix = 1;
passport.use(new MockStrategy({
	    name: 'mock1',
	    user: {
	    	idp_name: "University of Illinois at Chicago",
	    	eppn: "tuser" + mockUserSuffix + "@uic.edu",
	    	sub: "dummyUser" + (mockUserSuffix),
	    	given_name: "Test" + mockUserSuffix,
	    	family_name: "User" + mockUserSuffix,
	    	name: "Test User" + mockUserSuffix
	    }
	}, mockVerify));

mockUserSuffix = 2;
passport.use(new MockStrategy({
	    name: 'mock2',
	    user: {
	    	idp_name: "University of Illinois at Chicago",
	    	eppn: "tuser" + mockUserSuffix + "@uic.edu",
	    	sub: "dummyUser" + (mockUserSuffix),
	    	given_name: "Test" + mockUserSuffix,
	    	family_name: "User" + mockUserSuffix,
	    	name: "Test User" + mockUserSuffix
	    }
	}, mockVerify));

let idplist = require("./Data/idpList");

var redirect_uri = 'https://' + config.host + ':' + config.port + '/authcb';
console.log(redirect_uri, securityConfig.client_id, securityConfig.client_secret);
Issuer.discover('https://cilogon.org/.well-known/openid-configuration')
	.then(function (ciLogon) {
		var client = new ciLogon.Client({
			client_id: securityConfig.client_id,
			client_secret: securityConfig.client_secret,
			redirect_uri: [redirect_uri],
			scope: securityConfig.scope,
			response_type: "code"
		});
		client.CLOCK_TOLERANCE = 5 * 60; // 5 Minutes
		var params = {
			client_id: securityConfig.client_id,
			client_secret: securityConfig.client_secret,
			redirect_uri: redirect_uri,
			scope: securityConfig.scope,
			response_type: "code",
		};
		// Tell passport hows users will login
		passport.use('oidc', new Strategy({client: client, params: params}, verify));
		idplist.forEach(idp => {
			params.idphint = idp.value;
			passport.use('oidc' + idp.name, new Strategy({client: client, params: params}, verify));	
		})
		

		

		// handle requests
		//this.setUpRoutes();
		// For all paths, use this router
		//app.use('/', router);
	}.bind(this));







// Define all the routes here



//Non secure routes (Entry points)

const indexRouter = require("./routes/index");
app.use("/index", indexRouter);

//const usersRouter = require("./routes/users");
//app.use("/users", usersRouter);


// Secure routes

const homeRouter = require("./routes/home");
app.use("/home", homeRouter);

const adminRouter = require("./routes/admin");
app.use("/admin", adminRouter);

const testRouter = require("./routes/test");
app.use("/test", testRouter);

const shareEventRouter = require("./routes/shareevent");
app.use("/share", shareEventRouter);

//Authentication

app.get('/login', passport.authenticate('oidc'));

idplist.forEach(idp => {
	app.get('/login' + idp.name, passport.authenticate('oidc' + idp.name));
});
// authentication callback
app.get('/authcb', passport.authenticate('oidc'),  setUserCookies, redirectToHome);

app.get('/loginDummy1', passport.authenticate('mock1'), redirectToHome);
app.get('/loginDummy2', passport.authenticate('mock2'), redirectToHome);

app.get("/", (req, res) => {
	
	return res.redirect("/index");
});


app.get('/logout', function (req, res){
	req.session.destroy(function (err) {
		res.clearCookie('SAGE2.sid');
		res.redirect('/');
	});
});

/**
 * Express middleware for setting pointer name and pointer color
 *
 * @method setUserCookies
 * @param req {Object} the request object
 * @param res {Object} the response object
 * @param next {Object} the middleware chaining object
 */

function setUserCookies(req, res, next) {
	userDB.get(req.user.id, function (count, user) {
		if (count === 0) {
			return res.redirect('/login');
		}
		var ptrName = user.userinfo.given_name || req.user.SAGE2_ptrName;
		if (ptrName) {
			res.cookie('SAGE2_ptrName', ptrName);
		}
		if (req.user.SAGE2_ptrColor) {
			res.cookie('SAGE2_ptrColor', req.user.SAGE2_ptrColor);
		}
		next();
	});
}


global.createHash = function(str) {
	var hash = 0, i, chr;
	for (i = 0; i < str.length; i++) {
		chr   = str.charCodeAt(i);
		hash  = ((hash << 5) - hash) + chr;
		hash |= 0; // Convert to 32bit integer
	}
	return hash + "";
}