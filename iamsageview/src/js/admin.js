import React from "react";
import ReactDOM from "react-dom";
import WelcomeMessage from "js/components/WelcomeMessage";

import AllUsers from "js/components/AllUsers";
import CssBaseline from '@material-ui/core/CssBaseline';
import {useState, useEffect} from "react";


import "style/admin.css";






export default function Adminpage() {
    const [users, setUsers] = React.useState([]);
    
    const getUpdatedEventList = () => {
        fetch('/admin/allusers')
        .then(response => response.json())
        .then(data => {
            console.log(data.data);
            
            setUsers(data.data);
        });
    };
    useEffect(() => {
        getUpdatedEventList();
    }, []);
    return (
        <React.Fragment>
            <CssBaseline />
            <WelcomeMessage userId={findGetParameter("id")}/>
            <AllUsers className="centered" userList={users}/>
        </React.Fragment>
    );
}


ReactDOM.render(<Adminpage/>, document.getElementById("main"));

//Taken from: https://stackoverflow.com/questions/5448545/how-to-retrieve-get-parameters-from-javascript
function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}