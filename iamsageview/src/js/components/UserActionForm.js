import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import FormGroup from '@material-ui/core/FormGroup';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Box, List, ListItem, ListItemText, MenuItem, Radio, RadioGroup, FormControlLabel, FormControl, FormLabel, Checkbox }  from '@material-ui/core';
import { Paper, Table, TableCell, TableRow, TableHead, TableBody, TableContainer} from '@material-ui/core';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});
export default function UserActionForm({label, cleanUp, actionList, userActionTable, onSubmit}) {
    const [open, setOpen] = React.useState(false);
    const [roleName, setRoleName] = React.useState('Viewer');
    const [scheduleType, setScheduleType] = React.useState('No');
    const [accessType, setAccessType] = React.useState('All');
    var roleActionSet = {};

    
    const classes = useStyles();
    const handleEventNameChange = event => {
        setEventName(event.target.value);
    };
    const handleDisplayListClick = (event, index) => {
        setSelectedDisplayIndex(index);
    };
    const handleScheduleTypeChange = event => {
        setScheduleType(event.target.value);
    };
    const handleAccessTypeChange = event => {
        setAccessType(event.target.value);
    };
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleChange = (event) => {
        if (event.target.checked) {
            roleActionSet[event.target.name] = true;
        } else {
            delete roleActionSet[event.target.name];
        }
    }
    const handleClose = () => {
        setOpen(false);
      };
    const sendFormDataToServer = () => {
       

    };
    return (
        <div>
            <MenuItem onClick={ () => {handleClickOpen(); cleanUp(); }}>{label}</MenuItem>
                <Dialog open={open} onClose={handleClose} fullScreen fullWidth aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Edit user permissions</DialogTitle>
                    <DialogContent>
                    <Box pt={1}>
                        <FormControl component="fieldset">
                            <FormLabel component="legend">Actions:</FormLabel>
                            <TableContainer component={Paper}>
                                <Table className={classes.table}>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>User</TableCell>
                                            {
                                            actionList.map((action) =>(
                                                <TableCell>{action.action}</TableCell>
                                            ))}
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {userActionTable.map((row) => (
                                            <TableRow key={row.name}>
                                                <TableCell component="th" scope="row">
                                                    {row.name}
                                                </TableCell>
                                                {
                                                    actionList.map((action) => (
                                                        <TableCell>
                                                            <Checkbox
                                                                name={row.name + action.id}
                                                                checked={row.actionList.indexOf(action.id) > -1}
                                                                color="primary"
                                                            />
                                                        </TableCell>
                                                    ))
                                                }
                                            </TableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                             
                        </FormControl>
                    </Box>
                    
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Discard
          </Button>
          <Button onClick={() => {handleClose(); sendFormDataToServer();  onSubmit();}} color="primary">
            Save Changes
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
