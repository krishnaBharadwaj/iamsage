import React, {useState, useEffect} from "react";
import { GridList, GridListTile, Paper } from '@material-ui/core';
import UserCard from "js/components/UserCard";
import { positions, sizing } from '@material-ui/system';

export default function AllUsers({className, userList}) {
	return (<div className={className}>
				<GridList cellHeight="auto" cols={4} spacing={8}>
					{
						userList.map(userData => {
							return (
								<GridListTile key={userData.name} cols={1}>
									<UserCard userData={userData} />
								</GridListTile>
							)
						})
					}
				</GridList>
			</div>
		)
}
