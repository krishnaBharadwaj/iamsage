import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import FormGroup from '@material-ui/core/FormGroup';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Box, List, ListItem, ListItemText, MenuItem, Radio, RadioGroup, FormControlLabel, FormControl, FormLabel, Checkbox }  from '@material-ui/core';

export default function RoleDefForm({label, cleanUp, actionList, onSubmit}) {
    const [open, setOpen] = React.useState(false);
    const [roleName, setRoleName] = React.useState('Viewer');
    const [scheduleType, setScheduleType] = React.useState('No');
    const [accessType, setAccessType] = React.useState('All');
    var roleActionSet = {};
    const handleEventNameChange = event => {
        setRoleName(event.target.value);
    };
    const handleDisplayListClick = (event, index) => {
        setSelectedDisplayIndex(index);
    };
    const handleScheduleTypeChange = event => {
        setScheduleType(event.target.value);
    };
    const handleAccessTypeChange = event => {
        setAccessType(event.target.value);
    };
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleChange = (event) => {
        if (event.target.checked) {
            roleActionSet[event.target.name] = true;
        } else {
            delete roleActionSet[event.target.name];
        }
    }
    const handleClose = () => {
        setOpen(false);
      };
    const sendFormDataToServer = () => {
        var roleDetails = {
            name: roleName,
            actionList: Object.keys(roleActionSet),
        };
        console.log(roleDetails);
        fetch('/test/userroledef', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(roleDetails)
        })
        .then(res=>res.json())
        .then(res => console.log(res));

    };
    return (
        <div>
            <MenuItem onClick={ () => {handleClickOpen(); cleanUp(); }}>{label}</MenuItem>
                <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Create a new role</DialogTitle>
                    <DialogContent>
                    <Box pt={1}>
                        <FormControl component="fieldset">
                            <FormLabel component="legend">Name</FormLabel>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                defaultValue={roleName}
                                type="text"
                                onChange={handleEventNameChange}
                                fullWidth
                            />
                        </FormControl>
                    </Box>    

                    
                    <Box pt={1}>
                        <FormControl component="fieldset">
                            <FormLabel component="legend">Actions:</FormLabel>
                            <FormGroup>
                               {
                                actionList.map((action) =>(
                                    <FormControlLabel
                                        control={
                                          <Checkbox
                                            onChange={handleChange}
                                            name={action.id}
                                            color="primary"
                                          />
                                        }
                                        label={action.action}
                                      />
                                ))}
                            </FormGroup>
                        </FormControl>
                    </Box>
                    
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Discard
          </Button>
          <Button onClick={() => {handleClose(); sendFormDataToServer();  onSubmit();}} color="primary">
            Create
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
