import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function EventShareDialog({size, className, title, eventLink}) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const copyToClipBoard = () => {
  	let linkEl = document.getElementById("read-only-event-link");
  	linkEl.select();
  	linkEl.setSelectionRange(0, 99999);
  	document.execCommand("copy");
  };
  return (
		<div className={className}>
			<Button size={size}  onClick={handleClickOpen}>
				{title}
			</Button>
			<Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
				<DialogTitle id="form-dialog-title">Share Event</DialogTitle>
				<DialogContent>
					<DialogContentText>
						Users will be able to join the event using this link: 
					</DialogContentText>
					<TextField
						id="read-only-event-link"
						label="Event link"
						defaultValue={eventLink}
						InputProps={{
							readOnly: true,
						}}
						fullWidth
					/>
				</DialogContent>
				<DialogActions>
					<Button onClick={handleClose} color="primary">
						Cancel
					</Button>
					<Button onClick={()=>{copyToClipBoard(), handleClose()}} color="primary">
						Copy to Clipboard
					</Button>
				</DialogActions>
			</Dialog>
		</div>
  );
}