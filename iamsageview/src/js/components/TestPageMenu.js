import React, { Component } from "react";
import { Menu, MenuItem, Button}  from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import RoleDefForm from "js/components/RoleDefForm";
import UserActionForm from "js/components/UserActionForm";
import Dashboard from "js/components/Dashboard";
import "style/home.css";
var displayList = ["Cave2", "Continuum", "Back Wall"];
import {useState, useEffect} from "react";
export default function TestPageMenu ({eventFormOnSubmit}) {
	const [anchorEl, setAnchorEl] = React.useState(null);
	const [actionList, setActionList] = React.useState([]);
	const [userActionTable, setUserActionTable] = React.useState([]);
	const handleClick = event => {
		setAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		setAnchorEl(null);
	};

	useEffect(() => {
		fetch('/test/sageactions')
			.then(response => response.json())
			.then((data) => {
				console.log(data.data);
				setActionList(data.data);
			});

		fetch('/test/useractiontable')
			.then(response => response.json())
			.then((data) => {
				console.log(data.data);
				setUserActionTable(data.data)
			});
	}, []);
	return (
		<div className="menu">
			<Button aria-controls="HomeMenu" aria-haspopup="true" onClick={handleClick}>
		        <MenuIcon/>
		    </Button>
			<Menu
				id="HomeMenu"
				anchorEl={anchorEl}
				keepMounted
				open={Boolean(anchorEl)}
				onClose={handleClose}
			>
				<RoleDefForm label="Create Role" onSubmit={eventFormOnSubmit} cleanUp={handleClose} actionList={actionList} />
				<UserActionForm label="Edit user permissions" onSubmit={eventFormOnSubmit} cleanUp={handleClose} actionList={actionList} userActionTable={userActionTable} />
				<MenuItem onClick={handleClose}>Logout</MenuItem>
			</Menu>
		</div>
	);
	
}