import { Scheduler, WeekView, Appointments } from '@devexpress/dx-react-scheduler-material-ui';
import { ViewState } from '@devexpress/dx-react-scheduler';
import React, { useState, useEffect } from "react";
import Button from '@material-ui/core/Button';
var today = () => {
	var date = new Date();
	var dd = String(date.getDate()).padStart(2, '0');
	var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
	var yyyy = date.getFullYear();
	return yyyy + '-' + mm + '-' + dd;
}

export default function EventScheduler ({}){
	const [anchorEl, setAnchorEl] = React.useState("");
	const [scheduleButtonLabel, setSheduleButtonLabel] = React.useState("Schedule?");
	const [appointmentList, setAppointmentList] = React.useState([]);
	const handleClick = event => {
		setAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		setAnchorEl("");
	};
	useEffect(() => {
		fetch('/home/sageevents')
			.then(response => response.json())
			.then((data) => setAppointmentList(data.data));
	}, []);
	return (
		<React.fragment>
			<Button aria-controls="evtScheduler" aria-haspopup="true" onClick={handleClick} color="secondary">
		        {scheduleButtonLabel} 
		    </Button>
		</React.fragment>
	)
 
};