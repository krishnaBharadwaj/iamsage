import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Menu, MenuItem, Card, CardActions, CardContent, CardHeader, Avatar, IconButton} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import EventShareDialog from "js/components/EventShareDialog";
import MoreVertIcon from '@material-ui/icons/MoreVert';
import RoleDefForm from "js/components/RoleDefForm";
import UserActionForm from "js/components/UserActionForm";
const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  rightAligned: {
    marginLeft: 'auto'
  }
});
export default function EventCard({eventData, actionList, userActionTable, eventFormOnSubmit}) {
    const classes = useStyles();
    const bull = <span className={classes.bullet}>•</span>;
    

    
    
    function EventOptions(props){
        const [eventMenuAnchorEl, setEventMenuAnchorEl] = React.useState(null);

        const handleEventMenuClick = (event) => {
            setEventMenuAnchorEl(event.currentTarget);
        };

        const handleEventMenuClose = () => {
            setEventMenuAnchorEl(null);
        };
        return eventData.isOwner === true? (
            <React.Fragment>
                <IconButton aria-label="settings" aria-controls="eventManageMenu" aria-haspopup="true" onClick={handleEventMenuClick}>
                    <MoreVertIcon />
                </IconButton>
                <Menu
                    id="eventManageMenu"
                    anchorEl={eventMenuAnchorEl}
                    keepMounted
                    open={Boolean(eventMenuAnchorEl)}
                    onClose={handleEventMenuClose}
                >
                    
                    <RoleDefForm label="Create New Role" onSubmit={eventFormOnSubmit} cleanUp={handleEventMenuClose} actionList={actionList} />
                    <UserActionForm label="Edit user permissions" onSubmit={eventFormOnSubmit} cleanUp={handleEventMenuClose} actionList={actionList} userActionTable={userActionTable} />
                    <MenuItem onClick={handleEventMenuClose}>Delete Event</MenuItem>
                </Menu>
            </React.Fragment>
        ) : (
            <React.Fragment>
                <IconButton aria-label="settings" aria-controls="eventManageMenu" aria-haspopup="true" onClick={handleEventMenuClick}>
                    <MoreVertIcon />
                </IconButton>
                <Menu
                    id="eventManageMenu"
                    anchorEl={eventMenuAnchorEl}
                    keepMounted
                    open={Boolean(eventMenuAnchorEl)}
                    onClose={handleEventMenuClose}
                >
                    
                    <MenuItem onClick={handleEventMenuClose}>Leave Event</MenuItem>
                </Menu>
            </React.Fragment>
        );
    }
    
    return (

        <Card className={classes.root} variant="outlined">
            <CardHeader
                action={
                  <EventOptions/>  
                }
                title={eventData.name}
                subheader={"At " + eventData.display}
                titleTypographyProps={{variant:"h5"}}
            />
            <CardContent>
                <Typography color="textPrimary" gutterBottom>
                    {eventData.description}
                </Typography>
                <Typography variant="subtitle2" color="textSecondary">
                    {"By " + eventData.createdBy}
                </Typography>
            </CardContent>
            <CardActions>
                <EventShareDialog size="small" className={classes.rightAligned} title="Share" eventLink={eventData.shareUrl}/>
                <Button size="small" className={classes.rightAligned}>Participate</Button>
            </CardActions>
        </Card>
    );
}