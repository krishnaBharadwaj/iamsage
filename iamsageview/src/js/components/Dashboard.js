import React, {useState, useEffect} from "react";
import { GridList, GridListTile, Paper, Box, Typography } from '@material-ui/core';
import EventCard from "js/components/EventCard";
import { positions, sizing } from '@material-ui/system';

export default function Dashboard({className, eventListCollection, titleCollection, emptyMessage, actionList, userActionTable, eventFormOnSubmit}) {
	return emptyMessage.length > 0 ? (
			<div className={className}>
				<React.Fragment>
					<Typography className="titleText" variant="h4" color="inherit" gutterBottom>
			    		{emptyMessage}
					</Typography>
				</React.Fragment>
			</div>
		) : (
			<div className={className}>
				{
					eventListCollection
						.filter((eL) => {
							return eL.length > 0;
						})
						.map((eventList, i) => {
							return (
								<React.Fragment>
									<Box pb={4}>
										<Typography className="titleText" variant="h6" color="inherit" gutterBottom>
					        				{titleCollection[i]}
					    				</Typography>
										<GridList cellHeight="auto" cols={4} spacing={8}>
											{
												eventList.map(eventData => {
													return (
														<GridListTile key={eventData.name} cols={1}>
															<EventCard eventData={eventData} actionList={actionList} userActionTable={userActionTable} eventFormOnSubmit={eventFormOnSubmit}/>
														</GridListTile>
													)
												})
											}
										</GridList>
									</Box>
								</React.Fragment>
							)
					})
				}
			</div>
		)
}


