import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Box, List, ListItem, ListItemText, MenuItem, Radio, RadioGroup, FormControlLabel, FormControl, FormLabel }  from '@material-ui/core';

export default function EventForm({label, cleanUp, displayList, onSubmit}) {
    const [open, setOpen] = React.useState(false);
    const [eventName, setEventName] = React.useState('Meeting');
    const [description, setDescription] = React.useState('A generic SAGE event');
    const [scheduleType, setScheduleType] = React.useState('No');
    const [accessType, setAccessType] = React.useState('All');
    const [selectedDisplayIndex, setSelectedDisplayIndex] = React.useState(0);

    const handleEventNameChange = event => {
        setEventName(event.target.value);
    };
    const handleDescriptionChange = event => {
        setDescription(event.target.value);
    };
    const handleDisplayListClick = (event, index) => {
        setSelectedDisplayIndex(index);
    };
    const handleScheduleTypeChange = event => {
        setScheduleType(event.target.value);
    };
    const handleAccessTypeChange = event => {
        setAccessType(event.target.value);
    };
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
      };
    const setDefaults = () => {
        setEventName('Meeting');
        setDescription('A generic SAGE event');
        setScheduleType('No');
        setAccessType('All');
        setSelectedDisplayIndex(0);
    }
    const sendFormDataToServer = () => {
        var eventDetails = {
            name: eventName,
            description: description,
            scheduleType: scheduleType,
            schedule: "",
            accessType: accessType,
            display: displayList[selectedDisplayIndex]
        };
        console.log(eventDetails);
        fetch('/home/newsageevent', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(eventDetails)
        })
        .then(res=>res.json())
        .then(res => {
            console.log(res);
            onSubmit();
        });

    };
    return (
        <div>
            <MenuItem onClick={ () => {handleClickOpen(); cleanUp(); }}>{label}</MenuItem>
                <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Create a SAGE2 Event</DialogTitle>
                    <DialogContent>
                    <Box pt={1}>
                        <FormControl component="fieldset">
                            <FormLabel component="legend">Name</FormLabel>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                defaultValue={eventName}
                                type="text"
                                onChange={handleEventNameChange}
                                fullWidth
                            />
                        </FormControl>
                    </Box>    
                    <Box pt={1}>
                        <FormControl component="fieldset">
                            <FormLabel component="legend">Description</FormLabel>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="description"
                                defaultValue={description}
                                type="text"
                                onChange={handleDescriptionChange}
                                fullWidth
                            />
                        </FormControl>
                    </Box>    
                    <Box pt={1}>
                        <FormControl component="fieldset">
                            <FormLabel component="legend">Scheduled?</FormLabel>
                            <RadioGroup name="scheduleTypeRadio" value={scheduleType} onChange={handleScheduleTypeChange} inputProps={{ 'aria-label': 'Scheduled?' }} row>
                                <FormControlLabel
                                    value="No"
                                    control={<Radio color="variant" />}
                                    label="No"
                                    labelPlacement="end"
                                />
                                <FormControlLabel
                                    value="Yes"
                                    control={<Radio color="variant" />}
                                    label="Yes"
                                    labelPlacement="end"
                                />
                            </RadioGroup>
                        </FormControl>
                    </Box>
                    <Box pt={1}>
                        <FormControl component="fieldset">
                            <FormLabel component="legend">Event is open to:</FormLabel>
                            <RadioGroup  name="accessTypeRadio" value={accessType} onChange={handleAccessTypeChange} row>
                                <FormControlLabel
                                    value="All"
                                    control={<Radio color="variant" />}
                                    label="All"
                                    labelPlacement="end"
                                />
                                <FormControlLabel
                                    value="Org"
                                    control={<Radio color="variant" />}
                                    label="Anyone with org email"
                                    labelPlacement="end"
                                />
                                <FormControlLabel
                                    value="Private"
                                    control={<Radio color="variant" />}
                                    label="Selected users only (Private)"
                                    labelPlacement="end"
                                />
                            </RadioGroup>
                        </FormControl>
                    </Box>
                    <Box pt={1}>
                        <FormControl component="fieldset">
                            <FormLabel component="legend">Choose a display wall for this client</FormLabel>
                            <List component="nav" aria-label="display walls">
                                {
                                    (function() {
                                        return displayList.map((disp, i) => {
                                            return (<ListItem
                                                button
                                                divider
                                                selected={selectedDisplayIndex === i}
                                                onClick={event => handleDisplayListClick(event, i)}
                                            >
                                                <ListItemText primary={disp} />
                                            </ListItem>);
                                        });
                                    })()
                                    
                                }
                            </List>
                        </FormControl>
                    </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Discard
          </Button>
          <Button onClick={() => {handleClose(); sendFormDataToServer(); setDefaults();}} color="primary">
            Create
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
