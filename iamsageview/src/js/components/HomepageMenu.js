import React, { Component } from "react";
import { Menu, MenuItem, Button}  from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import EventForm from "js/components/EventForm";
import Dashboard from "js/components/Dashboard";
import "style/home.css";
var displayList = ["Cave2", "Continuum", "Back Wall"];
export default function HomePageMenu ({eventFormOnSubmit, onLogout}) {
	const [anchorEl, setAnchorEl] = React.useState(null);

	const handleClick = event => {
		setAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		setAnchorEl(null);
	};
	return (
		<div className="menu">
			<Button aria-controls="HomeMenu" aria-haspopup="true" onClick={handleClick}>
		        <MenuIcon/>
		    </Button>
			<Menu
				id="HomeMenu"
				anchorEl={anchorEl}
				keepMounted
				open={Boolean(anchorEl)}
				onClose={handleClose}
			>
				<EventForm label="Create Event" onSubmit={eventFormOnSubmit} cleanUp={handleClose} displayList={displayList} />
				<MenuItem onClick={()=> {handleClose(), onLogout()}}>Logout</MenuItem>
			</Menu>
		</div>
	);
	
}