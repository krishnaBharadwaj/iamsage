import React, { Component } from "react";
import "style/home.css";
import {Typography} from '@material-ui/core';
export default class WelcomeMessage extends Component {
	constructor(props) {
		super(props);
		console.log(props.userId);
		this.state = {
			userId: props.userId,
			name: ""
		};
	}
	componentDidMount() {
		var url = "/home/uname?id=" + this.state.userId;
		console.log(url);
		fetch(url)
			.then(response => response.json())
			.then(data => this.setState({
				name: data.data
			}));
	}
	render() {
		console.log(this.state.name);
		return (
			<div className="cushionedText">
				<Typography variant="h5" component="h5" color="textPrimary" gutterBottom>
                    Hi {this.state.name},
                </Typography>
			</div>
		);
	}
}