import React from "react";
import ReactDOM from "react-dom";
import WelcomeMessage from "js/components/WelcomeMessage";
import HomepageMenu from "js/components/HomepageMenu";
import Dashboard from "js/components/Dashboard";
import CssBaseline from '@material-ui/core/CssBaseline';
import {useState, useEffect} from "react";


import "style/home.css";



export default function Homepage(props) {
    const [events, setEvents] = React.useState([]);
    const [uid, setUid] = React.useState(findGetParameter("id"));
    const [ownedEvents, setOwnedEvents] = useState([]);
    const [otherEvents, setOtherEvents] = useState([]);
    const [actionList, setActionList] = React.useState([]);
    const [userActionTable, setUserActionTable] = React.useState([]);

    const getUpdatedEventList = () => {
        fetch('/home/sageevents?id=' + uid)
        .then(response => response.json())
        .then(data => {
            if (data.data && data.data.length > 0) {
                setEvents(data.data);
                console.log("Events:", events);
                setOwnedEvents(data.data.filter(x => {
                    return x.id.indexOf(uid) > -1;
                }).map(oe => {
                    oe.isOwner = true;
                    return oe;
                }));
                setOtherEvents(data.data.filter(x => {
                    return x.id.indexOf(uid) === -1;
                }));
            }
            
        });
    };
    const getSageActions = () => {
        fetch('/test/sageactions')
            .then(response => response.json())
            .then((data) => {
                console.log(data.data);
                setActionList(data.data);
            });
    };
    
    const getUserPermissions = () => {
        fetch('/test/useractiontable')
            .then(response => response.json())
            .then((data) => {
                console.log(data.data);
                setUserActionTable(data.data)
            });
    };

    const callLogout = () => {
        window.location.href = '/logout';
    };
    useEffect(() => {
        getUpdatedEventList();
        getSageActions();
        getUserPermissions();
    }, []);
    return (
        <React.Fragment>
            <CssBaseline />
            <WelcomeMessage userId={uid}/>
            <HomepageMenu eventFormOnSubmit={getUpdatedEventList} onLogout={callLogout} />
            <Dashboard 
                className="centered"
                actionList={actionList}
                userActionTable={userActionTable}
                eventFormOnSubmit={getUpdatedEventList}
                titleCollection={["Your Events", "Events you have access to"]}
                eventListCollection={[ownedEvents, otherEvents]}
                emptyMessage={events.length > 0 ? "" : "Events you create and other events you can access will appear here"}
            />
        </React.Fragment>
    );
}


ReactDOM.render(<Homepage/>, document.getElementById("main"));

//Taken from: https://stackoverflow.com/questions/5448545/how-to-retrieve-get-parameters-from-javascript
function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}