// This code is taken from the tutorial @ https://itnext.io/building-multi-page-application-with-react-f5a338489694
const fs = require("fs");
const path = require("path");

function getFilesFromDir(dir, fileTypes) {
	const returnFileList = [];
	function scanDir(curPath) {
		const files = fs.readdirSync(curPath);
		for (let i in files) {
			const curFile = path.join(curPath, files[i]);
			if (fs.statSync(curFile).isFile() && fileTypes.indexOf(path.extname(curFile)) !== -1) {
				returnFileList.push(curFile);
			} else if (fs.statSync(curFile).isDirectory()) {
				scanDir(curFile);
			}
		}
	}
	if (fs.statSync(dir).isDirectory()) {
		scanDir(dir);
	}
	return returnFileList;
}

function getDirsFromDir(dir, fileTypes) {
	const returnDirList = [];
	function scanDir(curPath) {
		const files = fs.readdirSync(curPath);
		for (let i in files) {
			const curFile = path.join(curPath, files[i]);
			if (fs.statSync(curFile).isDirectory()) {
				returnFileList.push(curFile);
			}
		}
	}
	if (fs.statSync(dir).isDirectory()) {
		scanDir(dir);
	}
	return returnDirList;
}


module.exports = {
	getFilesFromDir,
	getDirsFromDir
}
