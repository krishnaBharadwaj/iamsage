import React from "react";
import ReactDOM from "react-dom";
import WelcomeMessage from "js/components/WelcomeMessage";
import TestPageMenu from "js/components/TestPageMenu";
import Dashboard from "js/components/Dashboard";
import CssBaseline from '@material-ui/core/CssBaseline';
import {useState, useEffect} from "react";


import "style/test.css";






export default function Testpage() {
    const [events, setEvents] = React.useState([]);
    
    const getUpdatedEventList = () => {
        fetch('/home/sageevents')
        .then(response => response.json())
        .then(data => {
            console.log(data.data);
            
            setEvents(data.data);
        });
    };
    useEffect(() => {
        getUpdatedEventList();
    }, []);
    return (
        <React.Fragment>
            <CssBaseline />
            <TestPageMenu eventFormOnSubmit={getUpdatedEventList} />
        </React.Fragment>
    );
}


ReactDOM.render(<Testpage/>, document.getElementById("main"));

//Taken from: https://stackoverflow.com/questions/5448545/how-to-retrieve-get-parameters-from-javascript
function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}