import React from "react";
import ReactDOM from "react-dom";
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import {useState, useEffect} from "react";
import {entry_sage2} from "js/helpers/imagesLoader";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';


import "style/index.css";

export default function Index() {
	const [idpList, setIdpList] = useState([]);

	const getLoginOptions = () => {
		fetch('/index/options')
			.then(response => response.json())
			.then(data => setIdpList(data.data));
	};

	useEffect(() => {
		getLoginOptions();
	}, []);

	return (
		<div className="centered">
			<Grid container spacing={10} direction="column" alignItems="center" justify="center" style={{ minWidth: '30vw', minHeight: "70vh" }}>
				<Grid item xs={12}>
					<img src={entry_sage2} alt="Logo" />
				</Grid>
				<Grid item xs={12}>
					<Card raised>
						 <CardContent>
						 	<Box pb={4}>
								<Typography className="titleText" color="textSecondary" gutterBottom>
	                				Login using one of the following options:
	            				</Typography>
	            			</Box>
						 	<Grid container spacing={4} direction="column" alignItems="stretch" justify="center">
								{ 
									idpList.map(idp => {
										return (
											<Grid item lg={12}>
												<Button fullWidth variant="contained" color="primary" href={idp.value}>
						 							{idp.name}
												</Button>
											</Grid>
										);
									})
								}
							</Grid>
						</CardContent>
					</Card>
				</Grid>
			</Grid>
		</div>
    )
}
ReactDOM.render(<Index/>, document.getElementById("main"));

