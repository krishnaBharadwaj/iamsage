const path = require("path");
const {getFilesFromDir, getDirsFromDir} = require("./src/js/helpers/files");
const HtmlWebPackPlugin = require("html-webpack-plugin");

const SRC_DIR = "src";
const JS_DIR = path.join(SRC_DIR, "js");
const jsFiles = getFilesFromDir(JS_DIR, [".js"]);
const htmlFiles = getFilesFromDir(SRC_DIR, [".html"]);
var entry = jsFiles.reduce((obj, filePath) => {
	const entryName = filePath.replace(path.extname(filePath), "").replace(JS_DIR, "");
	obj[entryName] = "./" + filePath;
	return obj
}, {});


var alias = {};
alias.js = path.resolve(__dirname, "src", "js");
alias.assets = path.resolve(__dirname, "src", "assets");
alias.style = path.resolve(__dirname, "src", "style");



const htmlPlugins = htmlFiles.map( filePath => {
	const fileName = filePath.replace(SRC_DIR, "");
	return new HtmlWebPackPlugin({
		chunks: [fileName.replace(path.extname(fileName), ""), "vendor"],
		template: filePath,
		filename: fileName
	});
});

module.exports = {
	entry: entry,
	output: {
		filename: 'js/[name].js',
		path: path.resolve(__dirname, 'dist'),
		publicPath: "/"
	},
	plugins: [...htmlPlugins],
	resolve: {
		alias: alias
	},
	node: {
        fs: 'empty',
    },
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader",
					options: {
						presets: [
							"@babel/preset-env",
							"@babel/preset-react"
						]
					}
				}
			}, {
				test: /\.(png|svg|jpe?g|gif)$/,
				//type: 'asset/resource'
				loader: 'file-loader',
				options: {
					outputPath: (url, resourcePath, context) => {
            			return path.relative("src", resourcePath);
          			},
				},
			}, {
				test: /\.css$/i,
				use: ['style-loader', 'css-loader'],
			},
		]
	},
	optimization: {
		splitChunks: {
			cacheGroups: {
				vendor: {
					test: /node_modules/,
					chunks: "initial",
					name: "vendor",
					enforce: true
				}
			}
		}
	}
};